'''
WREP - Workflow for REPertoire data analysis
Copyright (C) 2016 Barbera DC van Schaik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from __future__ import print_function
import sqlite3
import sys


######### Functions ########

def create_table (cur, name, colnames):
    '''
    Description: create a table in the database
    In: name (table name), colnames (list with column names)
    Out: query (string)
    '''

    query = "DROP TABLE IF EXISTS " + name + ";"
    print(query)
    cur.execute(query)

    query = "CREATE TABLE " + name + " ("
    query = query + ", ".join(colnames)
    query = query + ");"
    print(query)
    cur.execute(query)
    return(query)

def import_data (con, cur, datafile, delim, table, colnames):
    '''
    Description: Reads a csv file and inserts the content in a table
    In: datafile (path), delim (e.g. ',' or ' '), table (name of the table in database)
    Out: query (string)
    '''
    fh = open(datafile, 'r')
    header = fh.readline()  # skip first line
    for row in fh:
        row = row.rstrip()
        row = row.replace('"', '')
        row = row.split(delim)
        query = "INSERT INTO " + table + " ("
        query = query + ", ".join(colnames)
        query = query + ") VALUES ("
        query = query + ", ".join(len(colnames) * ["?"]) + ");"
        # print(query, row)
        cur.execute(query, row)
    con.commit()
    fh.close()

def create_and_import (con, cur, table, datafile):
    '''
    Description: create a new table and import the data
    In: str tablename, str filename
    '''
    delim = "\t"

    # Get header and close the file again
    try:
        fh = open(datafile, "rb")
    except:
        sys.exit("cannot open file"+datafile)
    header = fh.readline()
    header = header.rstrip()
    header = header.replace(":1", "2")
    header = header.replace(".", "_")
    header = header.replace('"',"")
    fh.close()

    # Create table and fill table with data
    colnames = header.split(delim)
    if colnames[0] == "":
        colnames[0] = "someId"
    if "group" in colnames:
        colnames[colnames.index("group")] = "grp"
    print(colnames)
    create_table(cur, table,colnames)
    import_data(con, cur, datafile, delim, table, colnames)
