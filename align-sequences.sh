#!/bin/bash

# WREP - Workflow for REPertoire data analysis
# Copyright (C) 2016 Barbera DC van Schaik

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

####################################################

ref=$1    # hla_nuc.fasta
#fasta=$2  # sequences.fna
#qual=$3   # sequences.qual
fastq=$2  # sequences.fastq

#prefix=`basename ${fasta} .fasta`
mydir=`dirname ${fastq}`
prefix=`basename ${fastq} .fastq.gz`
refprefix=`basename ${ref} .fasta`

#echo "### convert fasta+qual to fastq KEEP FASTQ FILE ###"
#./fasta2fastq.pl -f ${fasta} -q ${qual} -o ${prefix}.fastq
#./fasta2fastq.pl -f ${fasta} -o ${prefix}.fastq
#wait
#gzip -f ${prefix}.fastq
#wait

echo "### align sequences with bwa ###"
./bwa-0.7.12/bwa mem ${ref} ${mydir}/${prefix}.fastq.gz > ${prefix}-${refprefix}.sam
wait

echo "### replace nucleotides that are identical with = ###"
samtools calmd -eS ${prefix}-${refprefix}.sam ${ref} > ${prefix}-${refprefix}-e.sam
wait
rm -f ${prefix}-${refprefix}.sam # REMOVE TMP FILE

echo "### fix CIGAR string KEEP THIS FILE ###"
java -jar picard-tools-1.126/picard.jar CleanSam I=${prefix}-${refprefix}-e.sam O=${prefix}-${refprefix}-e-clean.sam
wait
rm -f ${prefix}-${refprefix}-e.sam

echo "### convert sam to bam ###"
java -jar ./picard-tools-1.126/picard.jar SamFormatConverter I=${prefix}-${refprefix}-e-clean.sam O=${prefix}-${refprefix}.bam
wait

echo "### add read groups ###"
java -jar ./picard-tools-1.126/picard.jar AddOrReplaceReadGroups I=${prefix}-${refprefix}.bam O=${prefix}-${refprefix}RG.bam ID=a LB=a PL=454 PU=MID SM=a SO=coordinate
wait
rm -f ${prefix}-${refprefix}.bam

echo "### index bam file ###"
java -jar ./picard-tools-1.126/picard.jar BuildBamIndex I=${prefix}-${refprefix}RG.bam
wait

echo "### Create SAM output that is easy to import in R ###"
samtools view -F 0x04 ${prefix}-${refprefix}RG.bam | cut -f 1,2,3 > ${prefix}-${refprefix}-easy-import.txt

echo "### call raw variants ###"
samtools mpileup -f ${ref} ${prefix}-${refprefix}RG.bam > ${prefix}-${refprefix}RG.pileup
wait
rm -f ${prefix}-${refprefix}RG.bam ${prefix}-${refprefix}RG.bai

if [[ -s ${prefix}-${refprefix}RG.pileup ]]; then
    echo "### call variants that are covered by 1 read ###"
    java -jar VarScan.v2.3.7.jar pileup2snp ${prefix}-${refprefix}RG.pileup --min-coverage 1 --min-reads2 1 > ${prefix}-${refprefix}RG.snp.csv
    wait
else
    echo "Pilup file is empty. Skipped VarScan"
fi

rm -f ${prefix}-${refprefix}RG.pileup
